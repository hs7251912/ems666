package au.edu.sydney.domain;

import junit.framework.TestCase;

public class EventTest extends TestCase {
	private Event event;

	protected void setUp() throws Exception {
		event = new Event();
	}

	public void testSetAndGetEventName() {
		String testEventName = "aEventName";
		assertNull(event.getEventName());
		event.setEventName(testEventName);
		;
		assertEquals(testEventName, event.getEventName());
	}

	public void testSetAndGetOrganizer() {
		String testOrganizer = "anOrganizer";
		assertNull(event.getOrganizer());
		event.setOrganizer(testOrganizer);
		;
		assertEquals(testOrganizer, event.getOrganizer());
	}

	public void testSetAndGetCreateDate() {
		String testCreateDate = "aCreateDate";
		assertNull(event.getCreateDate());
		event.setCreateDate(testCreateDate);
		;
		assertEquals(testCreateDate, event.getCreateDate());
	}

	public void testSetAndGetDescription() {
		String testDescription = "aDescription";
		assertNull(event.getDescription());
		event.setDescription(testDescription);
		;
		assertEquals(testDescription, event.getDescription());
	}

	public void testSetAndGetLocation() {
		String testLocation = "aLocation";
		assertNull(event.getLocation());
		event.setLocation(testLocation);
		;
		assertEquals(testLocation, event.getLocation());
	}

	public void testSetAndGetStartDate() {
		String testStartDate = "aStartDate";
		assertNull(event.getStartDate());
		event.setStartDate(testStartDate);
		;
		assertEquals(testStartDate, event.getStartDate());
	}

	public void testSetAndGetEndDate() {
		String testEndDate = "aEndDate";
		assertNull(event.getEndDate());
		event.setEndDate(testEndDate);
		;
		assertEquals(testEndDate, event.getEndDate());
	}

	public void testSetAndGetStatus() {
		int testStatus = 1;
		assertEquals(0, 0, 0);
		event.setStatus(1);
		assertEquals(testStatus, event.getStatus(), 0);
	}

	public void testSetAndGetCapacity() {
		int testCapacity = 1;
		assertEquals(0, 0, 0);
		event.setCapacity(1);
		assertEquals(testCapacity, event.getCapacity(), 0);
	}

	public void testSetAndGetFees() {
		double testFees = 100.00;
		assertEquals(0, 0, 0);
		event.setFees(100.00);
		assertEquals(testFees, event.getFees(), 0);
	}

	public void testSetAndGetCategory() {
		String testCategory = "aCategory";
		assertNull(event.getCategory());
		event.setCategory(testCategory);
		;
		assertEquals(testCategory, event.getCategory());
	}

}
