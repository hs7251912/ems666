<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="http://getbootstrap.com/favicon.ico">

<title>Register</title>

<!-- Bootstrap core CSS -->
<link href="./static_resources/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="./static_resources/css/jumbotron.css" rel="stylesheet">

</head>

<body>

	<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
		<a class="navbar-brand" href="./">Event Bridge</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarsExampleDefault"
			aria-controls="navbarsExampleDefault" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarsExampleDefault">
			<ul class="navbar-nav mr-auto">
				<!-- <li class="nav-item active"> -->
				<li class="nav-item"><a class="nav-link" href="./">Home <span class="sr-only">(current)</span></a></li>
				<li class="nav-item"><a class="nav-link" href="./account/register">Sign Up</a></li>
				<li class="nav-item"><a class="nav-link" href="./account/login">Log in</a></li>
				<li class="nav-item"><a class="nav-link" href="./contact">Contact</a></li>
			</ul>
			<form class="form-inline my-2 my-lg-0">
				<input class="form-control mr-sm-2" type="text" placeholder="Search"
					aria-label="Search"> 
				<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
			</form>
		</div>
	</nav>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<div class="container">
			<!-- Example row of columns -->
			<form:form method="post" commandName="userForm" action="account/register">
				<table border="0" align="center">
					<tr>
						<td colspan="2" align="center"><h2><fmt:message key="register.heading"/></h2></td>
					</tr>
					<tr>
						<td width="33%">Username:</td>
						<td width="33%"><form:input path="username"/></td>
						<td width="34%"><form:errors path="username" cssClass="error"/></td>
					</tr>�
					<tr>
						<td width="33%">Password:</td>
						<td width="33%"><form:password path="password"/></td>
						<td width="34%"><form:errors path="password" cssClass="error"/></td>
					</tr>
					<tr>
						<td width="33%">Firstname:</td>
						<td width="33%"><form:input path="firstname"/></td>
						<td width="34%"><form:errors path="firstname" cssClass="error"/></td>
					</tr>
					<tr>
						<td width="33%">Lastname:</td>
						<td width="33%"><form:input path="lastname"/></td>
						<td width="34%"><form:errors path="lastname" cssClass="error"/></td>
					</tr>
					<tr>
						<td width="33%">DoB (YYYY-MM-DD):</td>
						<td width="33%"><form:input path="dob"/></td>
						<td width="34%"><form:errors path="dob" cssClass="error"/></td>
					</tr>
					<tr>
						<td width="33%">Phone:</td>
						<td width="33%"><form:input path="phone"/></td>
						<td width="34%"><form:errors path="phone" cssClass="error"/></td>
					</tr>
					<tr>
						<td width="33%">Email:</td>
						<td width="33%"><form:input path="email"/></td>
						<td width="34%"><form:errors path="email" cssClass="error"/></td>
					</tr>
					<tr>
						<td width="33%">Passport:</td>
						<td width="33%"><form:input path="passport"/></td>
						<td width="34%"><form:errors path="passport" cssClass="error"/></td>
					</tr>
					<tr>
						<td></td>
						<td>
							<button type="submit" >Submit</button>
							<button type="reset" >Cancel </button>
						</td>
					</tr>
				</table>
			</form:form>
		</div>
	</div>

	<div class="container">
		<!-- Example row of columns -->

		<hr>

		<footer>
			<p>� Company 2017</p>
		</footer>
	</div>
	<!-- /container -->


	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="./static_resources/js/jquery-3.2.1.slim.min.js"
		integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
		crossorigin="anonymous"></script>
	<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
	<script src="./static_resources/js/popper.min.js"></script>
	<script src="./static_resources/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="./static_resources/js/ie10-viewport-bug-workaround.js"></script>

</body>
</html>