package au.edu.sydney.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.edu.sydney.domain.Booking;
import au.edu.sydney.domain.BookingEventPair;
import au.edu.sydney.domain.Event;
import au.edu.sydney.domain.User;
import au.edu.sydney.service.BookingManager;
import au.edu.sydney.service.EventManager;

@Controller
@RequestMapping(value = "/booking")
public class BookingController {

	@Autowired
	private EventManager eventManager;

	@Autowired
	private BookingManager bookingManager;

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String viewEvent(Map<String, Object> model, HttpServletRequest request) {
		String username = request.getParameter("username");

		if (username != null) {

			List<Booking> bookings = bookingManager.retrieveBookings(username);
			List<BookingEventPair> bookingEventPairs = new ArrayList<BookingEventPair>();
			
			if (!bookings.isEmpty()) {
				for (Iterator<Booking> iterator = bookings.iterator(); iterator.hasNext();) {
					Booking booking = iterator.next();
					bookingEventPairs.add(new BookingEventPair(booking, eventManager.retrieveEvent(booking.getEventId())));
				}
				request.setAttribute("bookingEventPairs", bookingEventPairs);
			}

			return "my_bookings";
		}

		return null;
	}

	@RequestMapping(value = "/make", method = RequestMethod.GET)
	public String viewBooking(Map<String, Object> model, HttpServletRequest request) {
		if (request.getSession().getAttribute("user") != null) {
			int eventId = 0;
			try {
				eventId = Integer.parseInt(request.getParameter("eventId"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (eventId != 0) {
				request.setAttribute("event", eventManager.retrieveEvent(eventId));
				Booking booking = new Booking();
				model.put("booking", booking);
				return "booking_make";
			}
		}
		User userLogin = new User();
		request.setAttribute("userLogin", userLogin);
		return "user_login";
	}

	@RequestMapping(value = "/make", method = RequestMethod.POST)
	public String processBooking(@ModelAttribute("booking") Booking booking, HttpServletRequest request) {
		if (bookingManager.registerBooking(booking)) {
			int eventId = 0;
			try {
				eventId = Integer.parseInt(request.getParameter("eventId"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (eventId != 0) {
				request.setAttribute("event", eventManager.retrieveEvent(eventId));
				request.setAttribute("booking", bookingManager.retrieveBooking(booking.getUsername(), eventId));
				return "booking_success";
			}
		}
		return "booking_failure";
	}

}
