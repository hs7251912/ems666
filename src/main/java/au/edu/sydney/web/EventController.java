package au.edu.sydney.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.edu.sydney.domain.Event;
import au.edu.sydney.domain.EventAudio;
import au.edu.sydney.domain.MediaEventPair;
import au.edu.sydney.service.CategoryManager;
import au.edu.sydney.service.EventManager;
import au.edu.sydney.service.MediaManager;
import au.edu.sydney.utils.DateUtil;

@Controller
@RequestMapping(value = "/event")
public class EventController {

	@Autowired
	private EventManager eventManager;

	@Autowired
	private CategoryManager categoryManager;

	@Autowired
	private MediaManager mediaManager;

	@RequestMapping(value = "/post", method = RequestMethod.GET)
	public String viewPost(Map<String, Object> model, HttpServletRequest request) {
		Event eventForm = new Event();
		model.put("eventForm", eventForm);

		model.put("categoryNames", categoryManager.getCategoryNames());

		return "event_post";
	}

	@RequestMapping(value = "/post", method = RequestMethod.POST)
	public String processPost(@ModelAttribute("eventForm") Event event, HttpServletRequest request) {

		if (eventManager.postEvent(event)) {
			request.setAttribute("eventId",
					eventManager.retrieveEvent(event.getOrganizer(), event.getEventName()).getEventId());
			return "event_post_success";
		}

		return "event_post_failure";

	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String viewEvent(HttpServletRequest request) {
		int eventId = 0;
		try {
			eventId = Integer.parseInt(request.getParameter("eventId"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (eventId != 0) {
			Event event = eventManager.retrieveEvent(eventId);

			event.setStartDate(DateUtil.toYMDHMS(event.getStartDate()));
			event.setEndDate(DateUtil.toYMDHMS(event.getEndDate()));

			MediaEventPair mediaEventPair = new MediaEventPair(event, mediaManager.getEventImages(event.getEventId()), mediaManager.getEventAudios(event.getEventId()));
			request.setAttribute("mediaEventPair", mediaEventPair);

			return "event_home";
		}

		return null;
	}

	@RequestMapping(value = "/category", method = RequestMethod.GET)
	public String viewEventsByCategory(HttpServletRequest request) {
		String category = request.getParameter("category");
		
		if (category!=null)	{
			List<Event> events = eventManager.retrieveEventsByCategory(category);
			List<MediaEventPair> mediaEventPairs = new ArrayList<MediaEventPair>();
			for (Iterator<Event> iterator=events.iterator(); iterator.hasNext();) {
				Event event = iterator.next();
				mediaEventPairs.add(new MediaEventPair(event, mediaManager.getEventImages(event.getEventId()), mediaManager.getEventAudios(event.getEventId())));
			}
			request.setAttribute("category", category);
			request.setAttribute("mediaEventPairs", mediaEventPairs);
			return "events_by_category";
		}
		
		return null;
	}
}
