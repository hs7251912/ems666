package au.edu.sydney.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import au.edu.sydney.domain.User;
import au.edu.sydney.service.UserManager;
import au.edu.sydney.utils.DateUtil;

@Controller
@RequestMapping(value = "/account")
public class AccountController {

	@Resource
	HttpServletRequest request;
	
	@Autowired
	private UserManager userManager;

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String viewRegistration(Map<String, Object> model) {
		User userForm = new User();
		model.put("userForm", userForm);

		return "user_registration";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String processRegistration(@ModelAttribute("userForm") User user, HttpServletRequest request) {

		if (userManager.registerUser(user)) {
			User current_user = userManager.getUser(user.getUsername());
			current_user.setDob(DateUtil.toYMD(current_user.getDob()));
			request.getSession().setAttribute("user", current_user);
			return "user_registration_success";
		}

		return "user_registration_failure";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String viewLogin(Map<String, Object> model) {
		User userLogin = new User();
		model.put("userLogin", userLogin);

		return "user_login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String processLogin(@ModelAttribute("userLogin") User user, HttpServletRequest request) {

		if (userManager.loginUser(user)) {
			User current_user = userManager.getUser(user.getUsername());
			current_user.setDob(DateUtil.toYMD(current_user.getDob()));
			request.getSession().setAttribute("user", current_user);
			return "user_login_success";
		}

		return "user_login_failure";

	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String viewAccount() {
		return "account_home";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logOut(HttpServletRequest request) {
		request.getSession().removeAttribute("user");
		return "home";
	}
//The method will provide the medium to update an existing user
	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public String updateUser(HttpServletRequest request, ModelMap model){
		String username = request.getParameter("username");
		User user = userManager.getUser(username);
		if(username != null){
			user = userManager.getUser(username);
		}
		if(user != null) {
			return "account_set";
		}
		return null;
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String processUpdate(@RequestParam("password") String password, @RequestParam("newPassword1") String newPassword1,
			@RequestParam("newPassword2") String newPassword2,@RequestParam("phone") String phone,@RequestParam("email") String email,
			HttpServletRequest request) {
		Map<String, String> userInfo = new HashMap<String, String>();
		userInfo.put("password", password);
		userInfo.put("newPassword1", newPassword1);
		userInfo.put("newPassword2", newPassword2);
		userInfo.put("phone", phone);
		userInfo.put("email", email);
		
		String username = request.getParameter("username");
		User user = null;
		if(username != null){
			user = userManager.getUser(username);
		}
		if(user != null) {
			if(userManager.updateUser(user, userInfo)){
				request.getSession().setAttribute("user", user);
				return "account_home";
			}
			return "account_set_failure";
		}
		return null;
	}
	//list user account information
	 @RequestMapping(value="/account_list",method = RequestMethod.GET)
	    public String listUsers(ModelMap model, HttpServletRequest request) {
		 	List<User> users = userManager.findAllUser();
	        model.addAttribute("users", users);
	        return "account_list";
	    }
	 
	//delete user account
	@RequestMapping(value = "/account_delete", method = RequestMethod.GET)
	public String deleteAccount(HttpServletRequest request){
		String username = request.getParameter("username");
		userManager.deleteUser(username);
		//need to modify
	    return "redirect:./account_list";
	}

//	//admin controller
//	@RequestMapping("index")
//	public ModelAndView toLoginPage(){
//		return new ModelAndView("WEB-INF/jsp/login.jsp");
//	}
//	
//	@RequestMapping("login")
//	public ModelAndView doLogin(){
//		
//		String loginPageUrl = "WEB-INF/jsp/login.jsp";
//		String successPageUrl = "WEB-INF/jsp/success.jsp";
//		
//		String uname = request.getParameter("uname");
//		String upasswd = request.getParameter("upasswd");
//		
//		return userManager.doLogin(loginPageUrl, successPageUrl, uname, upasswd);
//	}
	
}
