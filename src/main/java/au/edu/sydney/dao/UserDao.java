package au.edu.sydney.dao;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import au.edu.sydney.domain.User;

@Repository(value = "userDao")
public class UserDao {

	@Resource
	private SessionFactory sessionFactory;
	
	private Session getSession(){
		return sessionFactory.getCurrentSession();
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void saveUser(User user) {
		sessionFactory.getCurrentSession().save(user);
	}

	public boolean isExistingUser(User user) {
		return getUser(user.getUsername()) != null;
	}

	public boolean usernamePasswordMatch(String username, String password) {
		return getUser(username, password) != null;
	}

	public User getUser(String username, String password) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Expression.like("username", username)).add(Expression.like("password", password));
		return (User) criteria.uniqueResult();
	}

	public User getUser(String username) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		criteria.add(Expression.like("username", username));
		return (User) criteria.uniqueResult();
	}
	//Search by username
	
	 public void addUser(User user) {
	        this.getSession().save(user);
	    }

	public boolean updateUser(User user, Map<String, String> userInfo) {
		// TODO Auto-generated method stub
		user.setPassword(userInfo.get("newPassword2"));
		user.setPhone(userInfo.get("phone"));
		user.setEmail(userInfo.get("email"));
		sessionFactory.getCurrentSession().update(user);
		return true;
	}
	public List<User> findAllUser(){
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
		List<User> users =(List<User>) criteria.list();
		return users;
	}
//	private User getUsername() {
//		return (User)sessionFactory.getCurrentSession().get(User.class, username);
//	}
	
		public void deleteUser(String username) {
		sessionFactory.getCurrentSession().delete(getUser(username));
	}



}
