package au.edu.sydney.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import au.edu.sydney.domain.Event;

@Repository(value = "eventDao")
public class EventDao {

	@Resource
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public void saveEvent(Event event) {
		sessionFactory.getCurrentSession().save(event);
	}

	public boolean isExistingEvent(Event event) {
		return getEvent(event.getOrganizer(), event.getEventName()) != null;
	}

	public Event getEvent(String organizer, String eventName) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Event.class);
		criteria.add(Expression.like("organizer", organizer)).add(Expression.like("eventName", eventName));
		return (Event) criteria.uniqueResult();
	}

	public Event getEvent(int eventId) {
		return (Event) sessionFactory.getCurrentSession().get(Event.class, eventId);
	}
	
	public List<Event> getEventsByOrganizer(String organizer) {
		return (List<Event>) sessionFactory.getCurrentSession().createCriteria(Event.class).add(Expression.like("organizer", organizer)).addOrder(Order.desc("createDate")).list();
	}
	
	public List<Event> getEventsByCategory(String category) {
		return (List<Event>) sessionFactory.getCurrentSession().createCriteria(Event.class).add(Expression.like("category", category)).addOrder(Order.desc("createDate")).list();
	}
	
	public List<Event> getLatestNEvents(int n) {
		return (List<Event>) sessionFactory.getCurrentSession().createCriteria(Event.class).addOrder(Order.desc("createDate")).setMaxResults(n).list();
	}
}
