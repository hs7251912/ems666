package au.edu.sydney.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import au.edu.sydney.dao.EventDao;
import au.edu.sydney.domain.Event;

@Service(value = "eventManager")
@Transactional
public class EventManager {

	@Autowired
	private EventDao eventDao;

	public boolean postEvent(Event event) {

		// Step 1: check whether this person is already in the database
		// Step 2: if not, save this person into the database
		if (!eventDao.isExistingEvent(event)) {
			eventDao.saveEvent(event);
			return true;
		}
		System.out.println("eventName already exits.");
		return false;
	}

	public Event retrieveEvent(String organizer, String eventName) {
		return eventDao.getEvent(organizer, eventName);
	}

	public Event retrieveEvent(int eventId) {
		return eventDao.getEvent(eventId);
	}
	
	public List<Event> retrieveEventsByOrganizer(String organizer) {
		return eventDao.getEventsByOrganizer(organizer);
	}
	
	public List<Event> retrieveEventsByCategory(String category) {
		return eventDao.getEventsByCategory(category);
	}
	
	public List<Event> retrieveLatestNEvents(int n) {
		return eventDao.getLatestNEvents(n);
	}
}
