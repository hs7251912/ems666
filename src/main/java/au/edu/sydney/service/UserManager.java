package au.edu.sydney.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;

import au.edu.sydney.dao.UserDao;
import au.edu.sydney.domain.User;

@Service(value = "userManager")
@Transactional
public class UserManager {

	@Autowired
	private UserDao userDao;

	// business logic of registering a Person into the database
	public boolean registerUser(User user) {

		// Step 1: check whether this person is already in the database
		// Step 2: if not, save this person into the database
		if (!userDao.isExistingUser(user)) {
			userDao.saveUser(user);
			return true;
		}
		System.out.println("username already exits.");
		return false;
	}

	public boolean loginUser(User user) {
		return userDao.usernamePasswordMatch(user.getUsername(), user.getPassword());
	}

	public User getUser(String username) {
		return userDao.getUser(username);
	}
	
	
	public void addUser(User user) {
	      userDao.addUser(user);
	    }
	
//	public void updateUser(User user){
//		User entity = userDao.getUser(user.getUsername());
//		if(entity!=null){
//			entity.setPassport(user.getPassword());
//			entity.setPhone(user.getPhone());
//			entity.setEmail(user.getEmail());
//		}
//	} 
	public boolean updateUser(User user, Map<String, String> userInfo) {
		return userDao.updateUser(user, userInfo);
	}
	
	public List<User> findAllUser() {
		return userDao.findAllUser();
	}
	
	public void deleteUser(String username) {
		userDao.deleteUser(username);
	}
	
public ModelAndView doLogin(String loginPageUrl, String successPageUrl, String uname, String upasswd) {
		
		if (uname == null || "".equals(uname)) {
			return new ModelAndView(loginPageUrl, "error", "用户名不能为空");
		}
		
		if (upasswd == null || "".equals(upasswd)) {
			return new ModelAndView(loginPageUrl, "error", "密码不能为空");
		}
		
		// uname = admin , passwd = 123
		
		if (uname.equals("admin") && upasswd.equals("123")) {
			return new ModelAndView(successPageUrl);
		}
		
		return new ModelAndView(loginPageUrl, "error", "用户名或者密码错误");
	}
}	
